###################################################################################
# Mentor Graphics Corporation
#
###################################################################################


##################
# Clocks
##################
create_clock  -name Design_Clock -period 2.500000
create_clock { rtlc_wb_s.clk } -name rtlc_wb_s.clk -period 2.500000

##################
# Input delays
##################
set_input_delay  -clock rtlc_wb_s.clk -add_delay 0.000 {rtlc_wb_s.adr[10] rtlc_wb_s.adr[11] rtlc_wb_s.adr[12] rtlc_wb_s.adr[2] rtlc_wb_s.adr[3] rtlc_wb_s.adr[4] rtlc_wb_s.adr[5] rtlc_wb_s.adr[6] rtlc_wb_s.adr[7] rtlc_wb_s.adr[8] rtlc_wb_s.adr[9] rtlc_wb_s.cti[*] rtlc_wb_s.dat_ms[*] rtlc_wb_s.sel[*] rtlc_wb_s.stb rtlc_wb_s.we}

###################
# Output delays
###################
set_output_delay  -clock Design_Clock -add_delay 0.000 {rtlc_wb_s.err rtlc_wb_s.rty}
set_output_delay  -clock rtlc_wb_s.clk -add_delay 0.000 {rtlc_wb_s.ack rtlc_wb_s.dat_sm[*]}
