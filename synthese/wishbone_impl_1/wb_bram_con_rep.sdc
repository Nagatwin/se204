###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name ram_processed -value "true" -instance mem/mem -design rtl 

set_attribute -name ram_processed -value "true" -instance mem_0/mem_0 -design rtl 

set_attribute -name ram_processed -value "true" -instance mem_1/mem_1 -design rtl 

set_attribute -name ram_processed -value "true" -instance mem_2/mem_2 -design rtl 



##################
# Clocks
##################
create_clock  -domain Design_Clock -name Design_Clock -period 2.500000 -waveform { 0.000000 1.250000 } -design rtl 
create_clock { rtlc_wb_s.clk } -domain ClockDomain0 -name rtlc_wb_s.clk -period 2.500000 -waveform { 0.000000 1.250000 } -design rtl 

##################
# Input delays
##################
set_input_delay 0.000 -clock Design_Clock -add_delay  -design rtl  {rtlc_wb_s.adr(0) rtlc_wb_s.adr(1) rtlc_wb_s.adr(13) rtlc_wb_s.adr(14) rtlc_wb_s.adr(15) rtlc_wb_s.adr(16) rtlc_wb_s.adr(17) rtlc_wb_s.adr(18) rtlc_wb_s.adr(19) rtlc_wb_s.adr(20) rtlc_wb_s.adr(21) rtlc_wb_s.adr(22) rtlc_wb_s.adr(23) rtlc_wb_s.adr(24) rtlc_wb_s.adr(25) rtlc_wb_s.adr(26) rtlc_wb_s.adr(27) rtlc_wb_s.adr(28) rtlc_wb_s.adr(29) rtlc_wb_s.adr(30) rtlc_wb_s.adr(31) rtlc_wb_s.bte(*) rtlc_wb_s.cyc rtlc_wb_s.rst}
set_input_delay 0.000 -clock rtlc_wb_s.clk -add_delay  -design rtl  {rtlc_wb_s.adr(10) rtlc_wb_s.adr(11) rtlc_wb_s.adr(12) rtlc_wb_s.adr(2) rtlc_wb_s.adr(3) rtlc_wb_s.adr(4) rtlc_wb_s.adr(5) rtlc_wb_s.adr(6) rtlc_wb_s.adr(7) rtlc_wb_s.adr(8) rtlc_wb_s.adr(9) rtlc_wb_s.cti(*) rtlc_wb_s.dat_ms(*) rtlc_wb_s.sel(*) rtlc_wb_s.stb rtlc_wb_s.we}

###################
# Output delays
###################
set_output_delay 0.000 -clock Design_Clock -add_delay  -design rtl  {rtlc_wb_s.err rtlc_wb_s.rty}
set_output_delay 0.000 -clock rtlc_wb_s.clk -add_delay  -design rtl  {rtlc_wb_s.ack rtlc_wb_s.dat_sm(*)}
