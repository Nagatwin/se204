###################################################################################
# Mentor Graphics Corporation
#
###################################################################################

#################
# Attributes
#################
set_attribute -name width_a -value "8" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name widthad_a -value "11" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name numwords_a -value "2048" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name outdata_reg_a -value "UNREGISTERED" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name address_aclr_a -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name outdata_aclr_a -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name indata_aclr_a -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name wrcontrol_aclr_a -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name byteena_aclr_a -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name width_byteena_a -value "1" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name width_b -value "8" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name widthad_b -value "11" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name numwords_b -value "2048" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name rdcontrol_reg_b -value "CLOCK1" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name address_reg_b -value "CLOCK1" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name outdata_reg_b -value "UNREGISTERED" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name outdata_aclr_b -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name rdcontrol_aclr_b -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name indata_reg_b -value "CLOCK1" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name wrcontrol_wraddress_reg_b -value "CLOCK1" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name byteena_reg_b -value "CLOCK1" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name indata_aclr_b -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name wrcontrol_aclr_b -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name byteena_aclr_b -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name width_byteena_b -value "1" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name address_aclr_b -value "NONE" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name byte_size -value "8" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name read_during_write_mode_mixed_ports -value "OLD_DATA" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name ram_block_type -value "AUTO" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name init_file -value "UNUSED" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name init_file_layout -value "UNUSED" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name maximum_depth -value "0" -instance -type integer mem/ix64056z17698 -design gatelevel 
set_attribute -name intended_device_family -value "Cyclone II" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name lpm_hint -value "UNUSED" -instance -type string mem/ix64056z17698 -design gatelevel 
set_attribute -name width_a -value "8" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name widthad_a -value "11" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name numwords_a -value "2048" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name outdata_reg_a -value "UNREGISTERED" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name address_aclr_a -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name outdata_aclr_a -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name indata_aclr_a -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name wrcontrol_aclr_a -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name byteena_aclr_a -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name width_byteena_a -value "1" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name width_b -value "8" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name widthad_b -value "11" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name numwords_b -value "2048" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name rdcontrol_reg_b -value "CLOCK1" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name address_reg_b -value "CLOCK1" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name outdata_reg_b -value "UNREGISTERED" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name outdata_aclr_b -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name rdcontrol_aclr_b -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name indata_reg_b -value "CLOCK1" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name wrcontrol_wraddress_reg_b -value "CLOCK1" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name byteena_reg_b -value "CLOCK1" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name indata_aclr_b -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name wrcontrol_aclr_b -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name byteena_aclr_b -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name width_byteena_b -value "1" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name address_aclr_b -value "NONE" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name byte_size -value "8" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name read_during_write_mode_mixed_ports -value "OLD_DATA" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name ram_block_type -value "AUTO" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name init_file -value "UNUSED" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name init_file_layout -value "UNUSED" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name maximum_depth -value "0" -instance -type integer mem/ix9899z17697 -design gatelevel 
set_attribute -name intended_device_family -value "Cyclone II" -instance -type string mem/ix9899z17697 -design gatelevel 
set_attribute -name lpm_hint -value "UNUSED" -instance -type string mem/ix9899z17697 -design gatelevel 

set_attribute -name width_a -value "8" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name widthad_a -value "11" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name numwords_a -value "2048" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name outdata_reg_a -value "UNREGISTERED" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name address_aclr_a -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name outdata_aclr_a -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name indata_aclr_a -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name wrcontrol_aclr_a -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name byteena_aclr_a -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name width_byteena_a -value "1" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name width_b -value "8" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name widthad_b -value "11" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name numwords_b -value "2048" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name rdcontrol_reg_b -value "CLOCK1" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name address_reg_b -value "CLOCK1" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name outdata_reg_b -value "UNREGISTERED" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name outdata_aclr_b -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name rdcontrol_aclr_b -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name indata_reg_b -value "CLOCK1" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name wrcontrol_wraddress_reg_b -value "CLOCK1" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name byteena_reg_b -value "CLOCK1" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name indata_aclr_b -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name wrcontrol_aclr_b -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name byteena_aclr_b -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name width_byteena_b -value "1" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name address_aclr_b -value "NONE" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name byte_size -value "8" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name read_during_write_mode_mixed_ports -value "OLD_DATA" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name ram_block_type -value "AUTO" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name init_file -value "UNUSED" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name init_file_layout -value "UNUSED" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name maximum_depth -value "0" -instance -type integer mem_0/ix64056z17696 -design gatelevel 
set_attribute -name intended_device_family -value "Cyclone II" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name lpm_hint -value "UNUSED" -instance -type string mem_0/ix64056z17696 -design gatelevel 
set_attribute -name width_a -value "8" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name widthad_a -value "11" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name numwords_a -value "2048" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name outdata_reg_a -value "UNREGISTERED" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name address_aclr_a -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name outdata_aclr_a -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name indata_aclr_a -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name wrcontrol_aclr_a -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name byteena_aclr_a -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name width_byteena_a -value "1" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name width_b -value "8" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name widthad_b -value "11" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name numwords_b -value "2048" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name rdcontrol_reg_b -value "CLOCK1" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name address_reg_b -value "CLOCK1" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name outdata_reg_b -value "UNREGISTERED" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name outdata_aclr_b -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name rdcontrol_aclr_b -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name indata_reg_b -value "CLOCK1" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name wrcontrol_wraddress_reg_b -value "CLOCK1" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name byteena_reg_b -value "CLOCK1" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name indata_aclr_b -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name wrcontrol_aclr_b -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name byteena_aclr_b -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name width_byteena_b -value "1" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name address_aclr_b -value "NONE" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name byte_size -value "8" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name read_during_write_mode_mixed_ports -value "OLD_DATA" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name ram_block_type -value "AUTO" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name init_file -value "UNUSED" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name init_file_layout -value "UNUSED" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name maximum_depth -value "0" -instance -type integer mem_0/ix9899z17695 -design gatelevel 
set_attribute -name intended_device_family -value "Cyclone II" -instance -type string mem_0/ix9899z17695 -design gatelevel 
set_attribute -name lpm_hint -value "UNUSED" -instance -type string mem_0/ix9899z17695 -design gatelevel 

set_attribute -name width_a -value "8" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name widthad_a -value "11" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name numwords_a -value "2048" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name outdata_reg_a -value "UNREGISTERED" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name address_aclr_a -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name outdata_aclr_a -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name indata_aclr_a -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name wrcontrol_aclr_a -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name byteena_aclr_a -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name width_byteena_a -value "1" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name width_b -value "8" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name widthad_b -value "11" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name numwords_b -value "2048" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name rdcontrol_reg_b -value "CLOCK1" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name address_reg_b -value "CLOCK1" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name outdata_reg_b -value "UNREGISTERED" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name outdata_aclr_b -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name rdcontrol_aclr_b -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name indata_reg_b -value "CLOCK1" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name wrcontrol_wraddress_reg_b -value "CLOCK1" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name byteena_reg_b -value "CLOCK1" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name indata_aclr_b -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name wrcontrol_aclr_b -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name byteena_aclr_b -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name width_byteena_b -value "1" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name address_aclr_b -value "NONE" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name byte_size -value "8" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name read_during_write_mode_mixed_ports -value "OLD_DATA" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name ram_block_type -value "AUTO" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name init_file -value "UNUSED" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name init_file_layout -value "UNUSED" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name maximum_depth -value "0" -instance -type integer mem_1/ix64056z17694 -design gatelevel 
set_attribute -name intended_device_family -value "Cyclone II" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name lpm_hint -value "UNUSED" -instance -type string mem_1/ix64056z17694 -design gatelevel 
set_attribute -name width_a -value "8" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name widthad_a -value "11" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name numwords_a -value "2048" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name outdata_reg_a -value "UNREGISTERED" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name address_aclr_a -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name outdata_aclr_a -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name indata_aclr_a -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name wrcontrol_aclr_a -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name byteena_aclr_a -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name width_byteena_a -value "1" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name width_b -value "8" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name widthad_b -value "11" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name numwords_b -value "2048" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name rdcontrol_reg_b -value "CLOCK1" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name address_reg_b -value "CLOCK1" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name outdata_reg_b -value "UNREGISTERED" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name outdata_aclr_b -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name rdcontrol_aclr_b -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name indata_reg_b -value "CLOCK1" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name wrcontrol_wraddress_reg_b -value "CLOCK1" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name byteena_reg_b -value "CLOCK1" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name indata_aclr_b -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name wrcontrol_aclr_b -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name byteena_aclr_b -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name width_byteena_b -value "1" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name address_aclr_b -value "NONE" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name byte_size -value "8" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name read_during_write_mode_mixed_ports -value "OLD_DATA" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name ram_block_type -value "AUTO" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name init_file -value "UNUSED" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name init_file_layout -value "UNUSED" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name maximum_depth -value "0" -instance -type integer mem_1/ix9899z17693 -design gatelevel 
set_attribute -name intended_device_family -value "Cyclone II" -instance -type string mem_1/ix9899z17693 -design gatelevel 
set_attribute -name lpm_hint -value "UNUSED" -instance -type string mem_1/ix9899z17693 -design gatelevel 

set_attribute -name width_a -value "8" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name widthad_a -value "11" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name numwords_a -value "2048" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name outdata_reg_a -value "UNREGISTERED" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name address_aclr_a -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name outdata_aclr_a -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name indata_aclr_a -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name wrcontrol_aclr_a -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name byteena_aclr_a -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name width_byteena_a -value "1" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name width_b -value "8" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name widthad_b -value "11" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name numwords_b -value "2048" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name rdcontrol_reg_b -value "CLOCK1" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name address_reg_b -value "CLOCK1" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name outdata_reg_b -value "UNREGISTERED" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name outdata_aclr_b -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name rdcontrol_aclr_b -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name indata_reg_b -value "CLOCK1" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name wrcontrol_wraddress_reg_b -value "CLOCK1" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name byteena_reg_b -value "CLOCK1" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name indata_aclr_b -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name wrcontrol_aclr_b -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name byteena_aclr_b -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name width_byteena_b -value "1" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name address_aclr_b -value "NONE" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name byte_size -value "8" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name read_during_write_mode_mixed_ports -value "OLD_DATA" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name ram_block_type -value "AUTO" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name init_file -value "UNUSED" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name init_file_layout -value "UNUSED" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name maximum_depth -value "0" -instance -type integer mem_2/ix64056z17692 -design gatelevel 
set_attribute -name intended_device_family -value "Cyclone II" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name lpm_hint -value "UNUSED" -instance -type string mem_2/ix64056z17692 -design gatelevel 
set_attribute -name width_a -value "8" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name widthad_a -value "11" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name numwords_a -value "2048" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name outdata_reg_a -value "UNREGISTERED" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name address_aclr_a -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name outdata_aclr_a -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name indata_aclr_a -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name wrcontrol_aclr_a -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name byteena_aclr_a -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name width_byteena_a -value "1" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name width_b -value "8" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name widthad_b -value "11" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name numwords_b -value "2048" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name rdcontrol_reg_b -value "CLOCK1" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name address_reg_b -value "CLOCK1" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name outdata_reg_b -value "UNREGISTERED" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name outdata_aclr_b -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name rdcontrol_aclr_b -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name indata_reg_b -value "CLOCK1" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name wrcontrol_wraddress_reg_b -value "CLOCK1" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name byteena_reg_b -value "CLOCK1" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name indata_aclr_b -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name wrcontrol_aclr_b -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name byteena_aclr_b -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name width_byteena_b -value "1" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name address_aclr_b -value "NONE" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name byte_size -value "8" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name read_during_write_mode_mixed_ports -value "OLD_DATA" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name ram_block_type -value "AUTO" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name init_file -value "UNUSED" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name init_file_layout -value "UNUSED" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name maximum_depth -value "0" -instance -type integer mem_2/ix9899z17691 -design gatelevel 
set_attribute -name intended_device_family -value "Cyclone II" -instance -type string mem_2/ix9899z17691 -design gatelevel 
set_attribute -name lpm_hint -value "UNUSED" -instance -type string mem_2/ix9899z17691 -design gatelevel 



##################
# Clocks
##################
create_clock  -domain Design_Clock -name Design_Clock -period 2.500000 -waveform { 0.000000 1.250000 } -design gatelevel 
create_clock { rtlc_wb_s.clk } -domain ClockDomain0 -name rtlc_wb_s.clk -period 2.500000 -waveform { 0.000000 1.250000 } -design gatelevel 

##################
# Input delays
##################
set_input_delay 0.000 -clock rtlc_wb_s.clk -add_delay  -design gatelevel  {rtlc_wb_s.adr(10) rtlc_wb_s.adr(11) rtlc_wb_s.adr(12) rtlc_wb_s.adr(2) rtlc_wb_s.adr(3) rtlc_wb_s.adr(4) rtlc_wb_s.adr(5) rtlc_wb_s.adr(6) rtlc_wb_s.adr(7) rtlc_wb_s.adr(8) rtlc_wb_s.adr(9) rtlc_wb_s.cti(*) rtlc_wb_s.dat_ms(*) rtlc_wb_s.sel(*) rtlc_wb_s.stb rtlc_wb_s.we}

###################
# Output delays
###################
set_output_delay 0.000 -clock Design_Clock -add_delay  -design gatelevel  {rtlc_wb_s.err rtlc_wb_s.rty}
set_output_delay 0.000 -clock rtlc_wb_s.clk -add_delay  -design gatelevel  {rtlc_wb_s.ack rtlc_wb_s.dat_sm(*)}
