//-----------------------------------------------------------------
// Wishbone BlockRAM
//-----------------------------------------------------------------
//
// Le paramètre mem_adr_width doit permettre de déterminer le nombre 
// de mots de la mémoire : (2048 pour mem_adr_width=11)

module wb_bram #(parameter mem_adr_width = 11) (
	// Wishbone interface
	wshb_if.slave wb_s
	);
	// a vous de jouer a partir d'ici
	
	// Le mode burst incrémental et burst constant sont gérés

	// Initialisation de la mémoire
	logic [3:0][7:0] mem [(2 ** mem_adr_width) - 1:0];

	// Adresse dans la mémoire de l'adresse demandée
	// (adr / 4) % 2 ** mem_adr_width
	logic [mem_adr_width-1:0] local_adr, next_adr;
	assign local_adr = wb_s.adr[mem_adr_width + 1:2];

	// On résoud combinatoirement l'adresse suivante, en cas de demande de
	// mode burst
	assign next_adr = (local_adr + 1);

	// Les signaux pour générer ack
	logic ACK_READ, ACK_WRITE;
	assign wb_s.ack = ACK_READ | ACK_WRITE;

	// On génère le ack de l'écriture combinatoirement
	assign ACK_WRITE = wb_s.stb & wb_s.we;

	// Valeurs à 0 pour err et retry, non supportés
	assign wb_s.rty = 0;
	assign wb_s.err = 0;

	always_ff @(posedge wb_s.clk)
	begin
		// On reset ack_read
		ACK_READ<=0;

		// On vérifie si il y a une opération
		if (wb_s.stb)
		begin
			// Si c'est une écriture
			if (wb_s.we)
			begin
				// On masque chacun des 4 octets pour les
				// écrire
				if (wb_s.sel[0])
					mem[local_adr][0] <= wb_s.dat_ms[7:0];
				if (wb_s.sel[1])
					mem[local_adr][1] <= wb_s.dat_ms[15:8];
				if (wb_s.sel[2])
					mem[local_adr][2] <= wb_s.dat_ms[23:16];
				if (wb_s.sel[3])
					mem[local_adr][3] <= wb_s.dat_ms[31:24];
			end

			// Si c'est une lecture
			else
			begin
				// On va lire la donnée sur ce cycle ou est en mode burst
				if (!ACK_READ || (wb_s.cti != 3'b0 && wb_s.cti != 3'b111))
					ACK_READ <= 1;

				// En cas de mode "incremental burst", si on a déjà
				// lu une donnée (ACK_READ = 1), on présente la
				// donnée suivante
				if (wb_s.cti == 3'b010 && ACK_READ == 1)
					wb_s.dat_sm <= mem[next_adr];

				// En mode normal ou burst constant, on présente la donnée
				else
					wb_s.dat_sm <= mem[local_adr];
			end
		end
	end
endmodule
