module dec7seg( I, O);
input [3:0] I;
output [6:0] O;
logic [6:0] O;

always_comb
	case(I)
		4'h0 : O = 7'b0111111 ;
		4'h1 : O = 7'b0000110 ;
		4'h2 : O = 7'b1011011 ;
		4'h3 : O = 7'b1001111 ;
		4'h4 : O = 7'b1100110 ;
		4'h5 : O = 7'b1101101 ;
		4'h6 : O = 7'b1111101 ;
		4'h7 : O = 7'b0000111 ;
		4'h8 : O = 7'b1111111 ;
		4'h9 : O = 7'b1100111 ;
		default: O=7'b0000000;
	endcase
endmodule
