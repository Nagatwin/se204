module dec7seg_tb();

logic [3:0] I;
wire [6:0] O;

dec7seg dut(.I(I), .O(O));
// dec7seg dut(.*);

initial
begin
	int i;
	for (i=0; i<5; i++)
	begin
		I=i;
		#10
		$display("Hi : %d %d",I, O);
	end
end


endmodule
