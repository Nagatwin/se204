`default_nettype none

module Top #(parameter HDISP = 800, VDISP = 480)
	(
    // Les signaux externes de la partie FPGA
	input  wire         FPGA_CLK1_50,
	input  wire  [1:0]	KEY,
	output logic [7:0]	LED,
	input  wire	 [3:0]	SW,
    // Les signaux du support matériel son regroupés dans une interface
    hws_if.master       hws_ifm,
    video_if.master video_ifm
);

//====================================
//  Déclarations des signaux internes
//====================================
  wire        sys_rst;   // Le signal de reset du système
  wire        sys_clk;   // L'horloge système a 100Mhz
  wire        pixel_clk; // L'horloge de la video 32 Mhz

//=======================================================
//  La PLL pour la génération des horloges
//=======================================================

sys_pll  sys_pll_inst(
		   .refclk(FPGA_CLK1_50),   // refclk.clk
		   .rst(1'b0),              // pas de reset
		   .outclk_0(pixel_clk),    // horloge pixels a 32 Mhz
		   .outclk_1(sys_clk)       // horloge systeme a 100MHz
);

//=============================
//  Les bus Wishbone internes
//=============================
wshb_if #( .DATA_BYTES(4)) wshb_if_sdram  (sys_clk, sys_rst);
wshb_if #( .DATA_BYTES(4)) wshb_if_stream (sys_clk, sys_rst);

//=============================
//  Le support matériel
//=============================
hw_support hw_support_inst (
    .wshb_ifs (wshb_if_sdram),
    .wshb_ifm (wshb_if_stream),
    .hws_ifm  (hws_ifm),
	.sys_rst  (sys_rst), // output
    .SW_0     ( SW[0] ),
    .KEY      ( KEY )
 );


//=============================
// On neutralise l'interface
// du flux video pour l'instant
// A SUPPRIMER PLUS TARD
//=============================
/*
assign wshb_if_stream.ack = 1'b1;
assign wshb_if_stream.dat_sm = '0 ;
assign wshb_if_stream.err =  1'b0 ;
assign wshb_if_stream.rty =  1'b0 ;
*/
//=============================
// On neutralise l'interface SDRAM
// pour l'instant
// A SUPPRIMER PLUS TARD
//=============================
/*
assign wshb_if_sdram.stb  = 1'b0;
assign wshb_if_sdram.cyc  = 1'b0;
assign wshb_if_sdram.we   = 1'b0;
assign wshb_if_sdram.adr  = '0  ;
assign wshb_if_sdram.dat_ms = '0 ;
assign wshb_if_sdram.sel = '0 ;
assign wshb_if_sdram.cti = '0 ;
assign wshb_if_sdram.bte = '0 ;
*/

// Le nombre de cycles pour chacun des 2 compteurs, plus faible si en
// simulation, 50M cycles : 100M cycles pour 1 période, donc 1 hertz
`ifdef SIMULATION
	localparam hcmpt = 50 ;
	localparam hcmpt2 = 16;
`else 
	localparam hcmpt = 50_000_000 ;
	localparam hcmpt2 = 16_000_000 ;
`endif

// Le signal de reset dans le domaine de pixel
logic pixel_rst;

// Les interfaces intermédiaires pour le bus wishbone
wshb_if #( .DATA_BYTES(4)) wshb_if_vga  (sys_clk, sys_rst);
//wshb_if #( .DATA_BYTES(4)) wshb_if_mire  (sys_clk, sys_rst);

// Le module VGA pour l'affichage
vga #(HDISP, VDISP) I_vga(.pixel_clk(pixel_clk), .pixel_rst(pixel_rst), .video_ifm(video_ifm), .wshb_ifm(wshb_if_vga.master));
//mire #(HDISP, VDISP) I_mire(.wshb_ifm(wshb_if_mire.master));
wshb_intercon I_intercon(.wshb_ifs_mire(wshb_if_stream.slave), .wshb_ifs_vga(wshb_if_vga.slave), .wshb_ifm(wshb_if_sdram.master));

// Les 2 bascules D en série
logic [1:0] pixel_gen;
assign pixel_rst = pixel_gen[1];

// On branche la LED 0 sur le bouton de reset
assign LED[0] = KEY[0];

// Les 2 compteurs pour les leds
logic [25:0] CTR;
logic [23:0] CTR2;

// Processus pour led1
always @(posedge sys_clk or posedge sys_rst)
if (sys_rst)
begin
	CTR <= 0;
	LED[1] <= 0;
end
else
begin
	// On change d'état tous les 50M passages (0.5s)
	if (CTR == hcmpt-1)
	begin
		CTR <= 0;
		LED[1] <= ~LED[1];
	end
	else
		CTR <= CTR + 1;
end

// Génération du reset synchronisé avec pixel_clk
always @(posedge pixel_clk or posedge sys_rst)
if (sys_rst)
	// Les 2 bascules a 1
	pixel_gen <= 2'b11;
else
	// On décale au tick de pixel_clk
	pixel_gen[1:0] <= {pixel_gen[0], 1'b0};

// On fait clignoter led2 avec la 2ème clock
always @(posedge pixel_clk or posedge pixel_rst)
if (pixel_rst)
begin
	CTR2 <= 0;
	LED[2] <= 0;
end
else
begin
	// On change d'état tout les 16M passages (0.5s)
	if (CTR2 == hcmpt2-1)
	begin
		CTR2 <= 0;
		LED[2] <= ~ LED[2];
	end
	else
		CTR2 <= CTR2 + 1;
end
endmodule
