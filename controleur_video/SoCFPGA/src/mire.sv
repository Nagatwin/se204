module mire #(parameter HDISP = 800, VDISP = 480)(
	wshb_if.master wshb_ifm
);

// On libère l'accès tous les 64 cycles
logic [5:0] wshb_free_ctr;

// On génère des requêtes en écriture en permanance
assign wshb_ifm.sel = 4'b1111;
assign wshb_ifm.cti = '0;
assign wshb_ifm.bte = '0;
assign wshb_ifm.cyc = wshb_ifm.stb;
assign wshb_ifm.we = 1'b1;
assign wshb_ifm.stb = (wshb_free_ctr != 0);

// Parametres pour l'écriture
localparam PXL_TOTAL =	HDISP * VDISP;
localparam PXL_BITS = $clog2(PXL_TOTAL);

// Compteur pour l'adresse courante
logic [PXL_BITS - 1 : 0] WRITE_cpt;
assign wshb_ifm.adr = 4 * WRITE_cpt;

// Mire à la volée : des colonnes blanches tout les 16 pixels
assign wshb_ifm.dat_ms = (WRITE_cpt[3:0] == '0) ? 32'h00ffffff : 32'h00000000;

// Le compteur pour libérer le bus. Pas besoin de remettre à 0 car sur 6 bits
always_ff @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
if (wshb_ifm.rst)
	wshb_free_ctr <= '0;
else
	wshb_free_ctr <= wshb_free_ctr + '1;

// Le compteur d'adresse pour les pixels
always_ff @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
if (wshb_ifm.rst)
	WRITE_cpt <= '0;
else
begin
	if (wshb_ifm.ack)
	begin
		if (WRITE_cpt == PXL_TOTAL - 1)
			WRITE_cpt <= '0;
		else
			WRITE_cpt <= WRITE_cpt + 1'b1;
	end
end

endmodule
