module wshb_intercon (
	wshb_if.slave wshb_ifs_mire,
	wshb_if.slave wshb_ifs_vga,
	wshb_if.master wshb_ifm
);

logic token;

// On alterne le token lorsque cyc est libre
always @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
if (wshb_ifm.rst)
	token = '0;
else
	if ((!wshb_ifs_vga.cyc && !token) || (!wshb_ifs_mire.cyc && token))
		token <= !token;	

// On branche les signaux autres => SDRAM suivant la valeur de token
assign wshb_ifm.stb = token ? wshb_ifs_mire.stb : wshb_ifs_vga.stb;
assign wshb_ifm.cyc = token ? wshb_ifs_mire.cyc : wshb_ifs_vga.cyc;
assign wshb_ifm.cti = token ? wshb_ifs_mire.cti : wshb_ifs_vga.cti;
assign wshb_ifm.adr = token ? wshb_ifs_mire.adr : wshb_ifs_vga.adr;
assign wshb_ifm.sel = token ? wshb_ifs_mire.sel : wshb_ifs_vga.sel;
assign wshb_ifm.we = token ? wshb_ifs_mire.we : wshb_ifs_vga.we;
assign wshb_ifm.bte = token ? wshb_ifs_mire.bte : wshb_ifs_vga.bte;
assign wshb_ifm.dat_ms = token ? wshb_ifs_mire.dat_ms : wshb_ifs_vga.dat_ms;

// On branche les signaux SDRAM => autres suivant la valeur de token, sans faire de X
assign wshb_ifs_vga.err = !token & wshb_ifm.err;
assign wshb_ifs_mire.err = token & wshb_ifm.err;
assign wshb_ifs_vga.rty = !token & wshb_ifm.rty;
assign wshb_ifs_mire.rty = token & wshb_ifm.rty;
assign wshb_ifs_vga.dat_sm = !token & wshb_ifm.dat_sm;
assign wshb_ifs_mire.dat_sm = token & wshb_ifm.dat_sm;
assign wshb_ifs_mire.ack = token & wshb_ifm.ack;
assign wshb_ifs_vga.ack = !token & wshb_ifm.ack;

endmodule
