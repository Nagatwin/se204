module vga #(parameter HDISP = 800, VDISP = 480) (
	input wire pixel_clk,
	input wire pixel_rst,
	video_if.master video_ifm,
	wshb_if.master wshb_ifm
);

// Paramètres donnés
localparam HFP = 40;
localparam HPULSE = 48;
localparam HBP = 40;
localparam VFP = 13;
localparam VPULSE = 3;
localparam VBP = 29;

// Paramètres pour générer BLANK
localparam HBLANK = HFP + HPULSE + HBP;
localparam VBLANK = VFP + VPULSE + VBP;

// Parametres pour le reset des compteurs
localparam HMAX = HBLANK + HDISP;
localparam VMAX = VBLANK + VDISP;

// Paramètres pour la taille des différents signaux
localparam VCPT_BITS = $clog2(VMAX);
localparam HCPT_BITS = $clog2(HMAX);
localparam VPXL_BITS = $clog2(VDISP);
localparam HPXL_BITS = $clog2(HDISP);

// Wires de la FIFO
logic has_been_full;
logic read, write, wfull, rempty, walmost_full;
logic [31:0] wdata, rdata;

// Les 2 compteurs (un vertical et un horizontal)
logic [VCPT_BITS - 1:0] V_cpt;
logic [HCPT_BITS - 1:0] H_cpt;

// 2 signaux combinatoires pour générer blank
logic is_vblank, is_hblank;
assign is_vblank = V_cpt < VBLANK;
assign is_hblank = H_cpt < HBLANK;

// On génère l'image de test à la volée
assign video_ifm.RGB = rdata[23:0];

// On assigne la clock
assign video_ifm.CLK = pixel_clk;

// On lit si ça a été plein une fois et que on est dans la zone d'affichage
assign read = (has_been_full && H_cpt >= HBLANK && V_cpt >= VBLANK);

// Logique synchrone pour générer les compteurs
always_ff @(posedge pixel_clk or posedge pixel_rst)
if (pixel_rst)
begin
	V_cpt <= '0;
	H_cpt <= '0;
end
else
begin
	if (H_cpt == HMAX - 1)
	begin
		// On a atteint la fin de la ligne, on passe à la ligne
		// suivante
		H_cpt <= '0;
		if (V_cpt == VMAX - 1)
			// On a atteint la fin de l'image, on reset les
			// 2 compteurs
			V_cpt <= '0;
		else
			V_cpt <= V_cpt + 1'b1;
	end
	else
		H_cpt <= H_cpt + 1'b1;
	
end

// On génère les signaux pour l'interface
// Intervalles pour HS et VS
assign video_ifm.HS = (H_cpt >= HFP && H_cpt < HFP + HPULSE) ? 1'b0 : 1'b1;
assign video_ifm.VS = (V_cpt >= VFP && V_cpt < VFP + VPULSE) ? 1'b0 : 1'b1;
	
// On assigne BLANK en fonction de la position des compteurs
assign video_ifm.BLANK = (is_vblank | is_hblank) ? 1'b0 : 1'b1;

// CODE SDRAM et fifo
async_fifo #(.DATA_WIDTH(32), .DEPTH_WIDTH(8), .ALMOST_FULL_THRESHOLD(224)) I_fifo(.rst(wshb_ifm.rst), .rclk(pixel_clk),
.read(read), .rdata(rdata), .rempty(rempty), .wclk(wshb_ifm.clk), .wdata(wdata), .write(write), .wfull(wfull), .walmost_full(walmost_full));

// On rééchantillonne wfull
logic wfull_wshb;
always @(posedge wshb_ifm.clk)
	wfull_wshb <= wfull;

logic [1:0] wfull_pxl_ech;
always @(posedge pixel_clk)
	wfull_pxl_ech <= {wfull_wshb, wfull_pxl_ech[1]};

// wfull dans le domaine pxl
logic wfull_pxl;
assign wfull_pxl = wfull_pxl_ech[0];

// Si wFull a été à 1 au moins une fois, on met à 1 le signal has_been_full
always @(posedge pixel_clk or posedge pixel_rst)
if (pixel_rst)
	has_been_full <= 1'b0;
else if (wfull_pxl)
	has_been_full <= 1'b1;

// Parametres pour la lecture de la SDRAM
localparam PXL_TOTAL =	HDISP * VDISP;
localparam PXL_BITS = $clog2(PXL_TOTAL);

// Génération de requêtes en continu sur le bus wishbone
assign wshb_ifm.dat_ms = 32'hBABECAFE;
assign wshb_ifm.sel = 4'b1111;
assign wshb_ifm.cti = '0;
assign wshb_ifm.bte = '0;

// Compteur pour l'adresse courante
logic [PXL_BITS - 1 : 0] READ_cpt;
assign wshb_ifm.adr = 4 * READ_cpt;

// Par défaut, on fait des lectures
//assign wshb_ifm.stb = wshb_ifm.cyc;
assign wshb_ifm.we = 1'b0;

// On lit que si on est pas plein
assign wshb_ifm.stb = (wfull == 0);
always_ff @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
if (wshb_ifm.rst)
    wshb_ifm.cyc <= '1;
else begin
    if (wfull)
        wshb_ifm.cyc <= '0;
    else if (!walmost_full)
        wshb_ifm.cyc <= '1;
end
    

// On écrit dans la FIFO uniquement lors de l'ack
assign write = wshb_ifm.ack;
assign wdata = wshb_ifm.dat_sm;

// Logique de génération du compteur pour la lecture dans la SDRAM
always_ff @(posedge wshb_ifm.clk or posedge wshb_ifm.rst)
if (wshb_ifm.rst)
	READ_cpt <= '0;
else
begin
	if (wshb_ifm.ack)
	begin
		if (READ_cpt == PXL_TOTAL - 1)
			READ_cpt <= '0;
		else
			READ_cpt <= READ_cpt + 1'b1;
	end
end

endmodule
