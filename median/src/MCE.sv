module MCE #(parameter NB_BITS = 8)
	(input [NB_BITS-1:0] A, B,
	output [NB_BITS-1:0] MAX, MIN);

// 2 assign avec ternaires sont suffisantes pour ce module
assign MIN = A > B ? B : A;
assign MAX = A > B ? A : B;

endmodule
