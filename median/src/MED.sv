module MED #(parameter NB_BITS = 8, NB_DATA = 9)
	(input [NB_BITS-1:0] DI,
	input DSI, BYP, CLK,
	output [NB_BITS-1:0] DO);

logic [NB_DATA-1:0][NB_BITS-1:0] R;
wire [NB_BITS-1:0] MIN, MAX;

// On assigne la sortie a la derniere bascule D
assign DO = R[NB_DATA-1];

// On utilise le module MCE
MCE #(NB_BITS)  I_MCE(.A(R[NB_DATA-1]), .B(R[NB_DATA-2]), .MIN(MIN), .MAX(MAX));

always_ff @(posedge CLK)
	// On utilise la concatenation pour faire la boucle entre les
	// registres
	R[NB_DATA-1:0] <= {(BYP ? R[NB_DATA-2] : MAX), (R[NB_DATA-3:0]), (DSI ? DI : MIN)};

endmodule
