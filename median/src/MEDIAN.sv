module MEDIAN #(parameter NB_BITS = 8)
	(input [NB_BITS-1:0] DI,
	input DSI, nRST, CLK,
	output [NB_BITS-1:0] DO,
	output logic DSO);

// Machine a etats, 
// RDY : prete a recevoir des donnees
// TR : transmission des donnees en cours
// SORT : tri en cours
// OUT : sortie disponible
enum logic[1:0] { RDY, TR, SORT, OUT } state;

// Signal BYP a generer
wire BYP;

// Pour generer l'alternance des valeurs de BYP pendant le tri
logic BYP_CLK;

// Un compteur pour le tri
logic [5:0] SORT_COUNT;

// On link BYP
assign BYP = BYP_CLK | DSI;

// On initialise le module med
MED #(NB_BITS) I_MED(.DI(DI), .DSI(DSI), .BYP(BYP), .CLK(CLK), .DO(DO));

always_ff @(posedge CLK or negedge nRST)
if(!nRST)
	state<=RDY;
else
// Calcul de l'etat du prochain tick
case(state)
	RDY : if (DSI) state <= TR;
	TR : if (!DSI) state <= SORT;
	SORT : if (SORT_COUNT == 38) state <= OUT;
	OUT : state <= RDY;
endcase


always_ff @(posedge CLK or negedge nRST)
if (!nRST)
begin
	// Par defaut, on a pas de sortie (DSO) et pas de tri en cours
	// (BYP_CLK)
	BYP_CLK <= 1'b0;
	DSO <= 1'b0;
	SORT_COUNT <= 6'b0;
end
else
begin
	if (state == RDY) begin
		// CF. reset
		BYP_CLK <= 1'b0;
		DSO <= 1'b0;
		SORT_COUNT <= 6'b0;
	end
	else if (state == TR) begin
		// Transmission en cours, aucune valeur n'est a changer
		BYP_CLK <= 1'b0;
		DSO <= 1'b0;
		SORT_COUNT <= 6'b0;
	end
	else if (state == SORT) begin
		// Tri en cours, tout est base sur le compteur de periodes
		// On met a 1, on aura juste besoin de materiel pour le mettre
		// a 0
		BYP_CLK <= 1'b0;

		// 7+1 periodes a 0 ecoulees (1 dans l'etat TR)
		// // 1 periode a 1
		if (SORT_COUNT == 7)
			BYP_CLK<=1'b1;
		
		// 7 periodes a 0
		// 2 periodes a 1
		else if (SORT_COUNT == 15 || SORT_COUNT == 16)
			BYP_CLK <= 1'b1;
		
		// 6 a 0, 3 a 1
		else if (SORT_COUNT < 26 && SORT_COUNT > 22)
			BYP_CLK <= 1'b1;
		
		// 5 a 0, 4 a 1
		else if (SORT_COUNT < 35 && SORT_COUNT > 30)
			BYP_CLK <= 1'b1;

		// Les 4 a 0 sont implicites
		
		// Pas de sortie
		DSO <= 0;

		// Le compteur
		SORT_COUNT <= SORT_COUNT + 1;
	end
	else if(state == OUT) begin
		// En mode sortie, on reset tout et on met DSO a 1
		BYP_CLK <= 0;
		DSO <= 1;
		SORT_COUNT <= 0;
	end
end

endmodule
